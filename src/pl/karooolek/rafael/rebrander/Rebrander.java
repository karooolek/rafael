package pl.karooolek.rafael.rebrander;

import javafx.util.Pair;

import java.io.File;
import java.io.IOException;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Rebrander {
    public final File srcFile;
    public final File dstFile;
    public final boolean ommitBinaryFiles;
    public final List<Pair<String, String>> replacements;

    public Rebrander(File srcFile, File dstFile, boolean ommitBinaryFiles, List<Pair<String, String>> replacements) {
        this.srcFile = srcFile;
        this.dstFile = dstFile;
        this.ommitBinaryFiles = ommitBinaryFiles;
        this.replacements = replacements;
    }

    public void rebrand() throws IOException {
        if (srcFile == null || !srcFile.exists()) {
            throw new RuntimeException("source directory " + srcFile.getAbsolutePath() + " does not exist");
        }
        if (Files.isHidden(srcFile.toPath()) || srcFile.getName().charAt(0) == '.') {
            return;
        }
        if (dstFile == null) {
            throw new RuntimeException("destination directory cannot be null");
        }
        File rebrandedDstFile = new File(doRebrand(this.dstFile.getPath() + "/" + srcFile.getName(), replacements));
        System.out.println("rebranding " + srcFile.toPath() + " -> " + rebrandedDstFile.toPath());
        doDelete(rebrandedDstFile);
        if (srcFile.isDirectory()) {
            Files.createDirectory(rebrandedDstFile.toPath());
            for (File file : srcFile.listFiles()) {
                new Rebrander(file, rebrandedDstFile, ommitBinaryFiles, replacements).rebrand();
            }
        } else {
            try {
                Files.readAllLines(srcFile.toPath());
//                lines = lines.stream().map(line -> doRebrand(line, replacements)).collect(Collectors.toList());
                String lines = new String(Files.readAllBytes(srcFile.toPath()));
                lines = doRebrand(lines, replacements);
                Files.createFile(rebrandedDstFile.toPath());
                Files.write(rebrandedDstFile.toPath(), lines.getBytes());
            } catch (MalformedInputException e) {
                if (ommitBinaryFiles) {
                    return;
                }
                // probably binary - just copy
                Files.copy(srcFile.toPath(), rebrandedDstFile.toPath());
            }
        }
    }

    private void doDelete(File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                doDelete(f);
            }
        }
        Files.delete(file.toPath());
    }

    private String doRebrand(String s, List<Pair<String, String>> replacements) {
        for (Pair<String, String> replacement : replacements) {
            s = s.replace(replacement.getKey(), replacement.getValue());
        }
        return s;
    }

    public static void main(String[] args) {
        File srcFile = new File(args[0]);
        File dstFile = new File(args[1]);
        boolean ommitBinaryFiles = Boolean.parseBoolean(args[2]);
        List<Pair<String, String>> replacements = new ArrayList<>();
        for (int i = 3; i + 1 <= args.length - 1; i += 2) {
            replacements.add(new Pair<>(args[i], args[i + 1]));
        }
        System.out.println("rebranding " + srcFile.toPath() + " -> (" + replacements + ") -> " + dstFile.getPath());
        try {
            new Rebrander(srcFile, dstFile, ommitBinaryFiles, replacements).rebrand();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
